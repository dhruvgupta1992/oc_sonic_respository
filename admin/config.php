<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/oc_sonic/admin/');
define('HTTP_CATALOG', 'http://localhost/oc_sonic/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/oc_sonic/admin/');
define('HTTPS_CATALOG', 'http://localhost/oc_sonic/');

// DIR
define('DIR_APPLICATION', 'C:/xampp/htdocs/oc_sonic/admin/');
define('DIR_SYSTEM', 'C:/xampp/htdocs/oc_sonic/system/');
define('DIR_IMAGE', 'C:/xampp/htdocs/oc_sonic/image/');
define('DIR_LANGUAGE', 'C:/xampp/htdocs/oc_sonic/admin/language/');
define('DIR_TEMPLATE', 'C:/xampp/htdocs/oc_sonic/admin/view/template/');
define('DIR_CONFIG', 'C:/xampp/htdocs/oc_sonic/system/config/');
define('DIR_CACHE', 'C:/xampp/htdocs/oc_sonic/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/xampp/htdocs/oc_sonic/system/storage/download/');
define('DIR_LOGS', 'C:/xampp/htdocs/oc_sonic/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/xampp/htdocs/oc_sonic/system/storage/modification/');
define('DIR_UPLOAD', 'C:/xampp/htdocs/oc_sonic/system/storage/upload/');
define('DIR_CATALOG', 'C:/xampp/htdocs/oc_sonic/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'oc_sonic');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
